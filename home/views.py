from django.shortcuts import render
from django.views import View
from django.contrib.auth import logout


class HomeView(View):
    def get(self, request):
        return render(request, 'home/home.html')


class LogMeOut(View):
    def get(self, request):
        logout(request)
        return render(request, 'home/home.html')

