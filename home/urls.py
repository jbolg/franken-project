from . import views
from django.urls import path
from .views import HomeView

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('log_me_out/', views.LogMeOut.as_view(), name='log_me_out'),
]