# Generated by Django 4.2.6 on 2023-11-19 14:15

import cloudinary_storage.storage
import cloudinary_storage.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("user_profile", "0004_alter_userprofile_user"),
    ]

    operations = [
        migrations.AddField(
            model_name="userprofile",
            name="video_clip",
            field=models.ImageField(
                blank=True,
                null=True,
                storage=cloudinary_storage.storage.VideoMediaCloudinaryStorage(),
                upload_to="videos/",
                validators=[cloudinary_storage.validators.validate_video],
            ),
        ),
    ]
